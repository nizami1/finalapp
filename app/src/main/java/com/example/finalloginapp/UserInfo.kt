package com.example.finalloginapp

data class UserInfo(
    val name: String = "",
    val url: String = "",
    val age: String = "",
    val mail: String = "",
)
