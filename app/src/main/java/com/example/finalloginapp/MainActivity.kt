package com.example.finalloginapp

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.example.finalloginapp.fragments.RegisterDialog
import com.example.finalloginapp.fragments.ResetDialog
import com.google.firebase.auth.FirebaseAuth

class MainActivity : AppCompatActivity() {

    private lateinit var mail: EditText
    private lateinit var pass: EditText
    private lateinit var loginButton: Button
    private lateinit var registerButton: Button
    private lateinit var resetButton: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if(FirebaseAuth.getInstance().currentUser != null){
            gotoLogged()
        }
        setContentView(R.layout.activity_main)

        init()
        loginListeners()
        registerListeners()
        resetListeners()
    }

    private fun init() {
        mail = findViewById(R.id.mail)
        pass = findViewById(R.id.pass)
        loginButton = findViewById(R.id.loginButton)
        registerButton = findViewById(R.id.registerButton)
        resetButton = findViewById(R.id.resetButton)
    }

    private fun loginListeners(){
        loginButton.setOnClickListener {

            val email = mail.text.toString()
            val passw = pass.text.toString()

            if(email.isEmpty() || passw.isEmpty()){
                Toast.makeText(this, "One of the fields is empty !", Toast.LENGTH_SHORT).show()
            }else{
                FirebaseAuth.getInstance().signInWithEmailAndPassword(email,passw)
                    .addOnCompleteListener{ task ->
                        if(task.isSuccessful){
                            gotoLogged()
                        } else{
                            Toast.makeText(this, "Login Unsuccessful", Toast.LENGTH_SHORT).show()
                        }

                    }
            }
        }
    }

    private fun registerListeners(){
        registerButton.setOnClickListener{
            val dialog = RegisterDialog(this)
            dialog.show()
        }
    }

    private fun resetListeners(){
        resetButton.setOnClickListener{
            val dialog = ResetDialog(this)
            dialog.show()

        }
    }

    private fun gotoLogged(){
        startActivity(Intent(this, LoggedActivity::class.java))
        finish()
    }

}