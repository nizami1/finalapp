package com.example.finalloginapp.adapters

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.example.finalloginapp.fragments.PageOne
import com.example.finalloginapp.fragments.PageTwo

class ViewPagerAdapter(activity: FragmentActivity): FragmentStateAdapter(activity) {

    override fun getItemCount() = 2

    override fun createFragment(position: Int): Fragment {
        return when(position){
            0 -> PageOne()
            1 -> PageTwo()
            else -> PageOne()
        }

    }
}