package com.example.finalloginapp.fragments

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.util.Patterns
import android.widget.Button
import android.widget.EditText
import com.example.finalloginapp.R
import com.google.firebase.auth.FirebaseAuth

class RegisterDialog(context: Context): Dialog(context) {
    private lateinit var mailD: EditText
    private lateinit var passD: EditText
    private lateinit var passDRepeat: EditText
    private lateinit var registerButtonDialog: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.dialog_registration)
        init()
        registerListener()


    }
    private fun registerListener(){
        registerButtonDialog.setOnClickListener {
            val mail = mailD.text.toString()
            val pass = passD.text.toString()
            val passR = passDRepeat.text.toString()

            mailCheck(mail)

            when {
                !mailCheck(mail) || mail.isEmpty() -> {
                    mailD.error = "Enter Valid E-mail !"

                }
                pass != passR || passR.isEmpty() -> {
                    passDRepeat.error = "Passwords don't match !"
                }
                pass.length < 8 -> {
                    passD.error = "Password should be at least 8 characters"
                }
                pass.length >= 8 && pass == passR && mailCheck(mail) -> {

                    FirebaseAuth.getInstance().createUserWithEmailAndPassword(mail,pass)
                        .addOnCompleteListener {task ->
                            if(task.isSuccessful){
                                dismiss()
                            }

                        }
                }

            }


        }
    }
    private fun mailCheck(mail: String): Boolean{
        return mail.isNotEmpty() && Patterns.EMAIL_ADDRESS.matcher(mail).matches()
    }

    private fun init(){
        mailD = findViewById(R.id.mailD)
        passD = findViewById(R.id.passD)
        passDRepeat = findViewById(R.id.passDRepeat)
        registerButtonDialog = findViewById(R.id.registerButtonDialog)
    }
}