package com.example.finalloginapp.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.View
import androidx.viewpager2.widget.ViewPager2
import com.example.finalloginapp.R
import com.example.finalloginapp.adapters.ViewPagerAdapter
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator

class ThirdFragment : Fragment(R.layout.fragment_third) {

    private lateinit var tabLayout: TabLayout
    private lateinit var viewPager: ViewPager2
    private lateinit var viewPagerAdapter: ViewPagerAdapter

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        tabLayout = requireView().findViewById(R.id.tabLayout)
        viewPager = requireView().findViewById(R.id.viewPager)
        viewPagerAdapter =  ViewPagerAdapter(requireActivity())

        viewPager.adapter = viewPagerAdapter
        TabLayoutMediator(tabLayout,viewPager){tab, position ->
            tab.text = "page ${position+1}"
        }.attach()
    }

}