package com.example.finalloginapp.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.View
import android.widget.CalendarView
import android.widget.TextView
import com.example.finalloginapp.R

class SecondFragment : Fragment(R.layout.fragment_second) {

    private lateinit var date: TextView
    private lateinit var calendarView: CalendarView
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        init()

        calendarView.setOnDateChangeListener(CalendarView.OnDateChangeListener { _, year, month, dayOfMonth ->
            val datte = dayOfMonth.toString() + "-" + (month + 1) + "-" + year
            date.text = datte
        })


    }

    private fun init() {
        calendarView = requireView().findViewById(R.id.calendarView)
        date = requireView().findViewById(R.id.date)
    }
}