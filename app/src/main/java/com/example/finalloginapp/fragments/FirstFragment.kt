package com.example.finalloginapp.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.View
import android.widget.*
import com.bumptech.glide.Glide
import com.example.finalloginapp.R
import com.example.finalloginapp.UserInfo
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener


class FirstFragment : Fragment(R.layout.fragment_first_fragments) {

    private lateinit var image: ImageView
    private lateinit var imageLink: EditText
    private lateinit var nameText: TextView
    private lateinit var mailText: TextView
    private lateinit var ageText: TextView
    private lateinit var editName: EditText
    private lateinit var mailF: EditText
    private lateinit var age: EditText
    private lateinit var saveButton: Button
    private lateinit var logoutButton: Button

    private val auth = FirebaseAuth.getInstance()
    private val db = FirebaseDatabase.getInstance().getReference("UserInfo")



    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        init()

        saveListener()

        logoutListener()

        db.child(auth.currentUser?.uid!!).addValueEventListener(object : ValueEventListener{
            override fun onDataChange(snapshot: DataSnapshot) {


                val name = editName.text.toString()
                val age = age.text.toString()
                val mail = mailF.text.toString()

                nameText.text = ("Name is: $name")
                ageText.text = ("Age is: $age")
                mailText.text = ("E-mail is: $mail")



                val userInfo: UserInfo = snapshot.getValue(UserInfo::class.java) ?: return

                Glide.with(this@FirstFragment)
                    .load(userInfo.url).placeholder(R.drawable.ic_one)
                    .into(image)

            }

            override fun onCancelled(error: DatabaseError) {
                TODO("Not yet implemented")
            }
        })

    }

    private fun logoutListener(){
        logoutButton.setOnClickListener {
            auth.signOut()
        }
    }

    private fun init(){
        image = requireView().findViewById(R.id.image)
        imageLink = requireView().findViewById(R.id.imageLink)
        nameText = requireView().findViewById(R.id.nameText)
        mailText = requireView().findViewById(R.id.mailText)
        ageText = requireView().findViewById(R.id.ageText)
        editName = requireView().findViewById(R.id.editName)
        mailF = requireView().findViewById(R.id.mailF)
        age = requireView().findViewById(R.id.age)
        saveButton = requireView().findViewById(R.id.saveButton)
        logoutButton = requireView().findViewById(R.id.logoutButton)

    }

    private fun saveListener(){
        saveButton.setOnClickListener {
            val name = editName.text.toString()
            val url = imageLink.text.toString()
            val age = age.text.toString()
            val mail = mailF.text.toString()

            val userInfo = UserInfo(name,url,age,mail)

            db.child(auth.currentUser?.uid!!).setValue(userInfo)
        }
    }

}