package com.example.finalloginapp.fragments

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import com.example.finalloginapp.R
import com.google.firebase.auth.FirebaseAuth

class ResetDialog(context: Context): Dialog(context) {
    private lateinit var resetMail: EditText
    private lateinit var buttonReset: Button
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.dialog_reset)
        init()
        resetListener()
    }
    private fun resetListener(){
        buttonReset.setOnClickListener {
            val mail = resetMail.text.toString()

            FirebaseAuth.getInstance()
                .sendPasswordResetEmail(mail)
                .addOnCompleteListener{task ->
                    if(task.isSuccessful){
                        dismiss()
                    }
                }
        }
    }

    private fun init(){
        resetMail = findViewById(R.id.resetMail)
        buttonReset = findViewById(R.id.buttonReset)
    }
}